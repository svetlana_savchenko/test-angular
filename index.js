import http from 'http';
import staticServer from 'node-static';

let file = new staticServer.Server('./app/views');
staticServer.publicPath = './build'

http.createServer(function(req, res) {
  file.serve(req, res);
}).listen(8080);

console.log('Server running on port 8080');