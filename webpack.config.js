var webpack = require("webpack"),
  ExtractTextPlugin = require('extract-text-webpack-plugin'),
  path = require('path');

var ROOT_PATH = path.join(__dirname);
var APP = '/app';

var LESS_LOADER = 'style!css!less';
var CSS_LOADER = 'style!css';

if (process.env.NODE_ENV === 'production') {
  LESS_LOADER = ExtractTextPlugin.extract('style', 'css!less?sourceMap');
  CSS_LOADER = ExtractTextPlugin.extract('style', 'css?minimize');
}

module.exports = {
  context: __dirname + '/app',
  entry: {
    index: "./scripts/index.js"
    },
    output: {
      path: "build",
      publicPath: "/",
      filename: "js/[name].js"
    },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: (/node_modules/),
        query: {
          presets: ['es2015']
        }
      },
      { test: /\.html$/, loaders: ['ngtemplate?relativeTo=' + path.resolve(ROOT_PATH, APP), 'html?root=' + APP] },
      { test: /\.less$/, loader: LESS_LOADER },
      { test: /\.css$/, loader: CSS_LOADER },
      { test: /\.(png|jpg|gif|eot|svg|ttf|woff|woff2|otf)$/, loader: 'file-loader?name=css/fonts/[name].[ext]&limit=10000' }
    ]
  },
  plugins: [
    new ExtractTextPlugin("css/dist/[name].css")
  ],
  resolve: {
    moduleDirectories: ['node_modules'],
    extensions: ['', '.js', '.json', '.png', '.gif', '.less', '.css', '.min.js']
  },
  resolveLoader: {
    moduleDirectories: ['node_modules'],
    moduleTemplates: ['*-loader', '*'],
    extentions: ['', '.js']
  },

  devServer: {
    host: 'localhost',
    port: 8080,
    contentBase: __dirname + '/app'
  }
}