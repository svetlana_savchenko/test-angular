// angular and ng* plugins
import angular from 'angular';
import ngRoute from 'angular-route';
import confirm from 'angular-ui-bootstrap/src/modal';
import 'ng-notify/dist/ng-notify';
import 'ng-notify/dist/ng-notify.min.css';
import '../styles/main.less';

// modules
import Components from '../components';
import Directives from '../directives';
import Services from '../services';

export default angular
  .module('TestAngular', [
    'ngNotify',
    confirm,
    ngRoute,
    Services,
    Directives,
    Components
  ])