/**
 * FormGroupController
 * @constructor
 */
function FormGroupController() {
  let vm = this;

  vm.regExpr = getRegExpr;
  vm.getCssClasses = getCssClass;


  /**
   * Get regular expression for validators
   * @returns {{phone: RegExp}}
   */
  function getRegExpr() {
    return {
      phone: /^\s*(?:\+?(\d{1,3}))?([- (]*(\d{3})[- )]*)?((\d{3})[- ]*(\d{2,4})(?:[-x ]*(\d+))?)\s*$/i
    }
  }

  /**
   * Get error or success class
   * @param ngModelContoller
   * @returns {{has-error: (boolean|FormController.$invalid|*|ngModel.NgModelController.$invalid|context.ctrl.$invalid|Jd.$invalid), has-success: (boolean|FormController.$valid|*|ngModel.NgModelController.$valid|context.ctrl.$valid|Jd.$valid)}}
   */
  function getCssClass(ngModelContoller) {
    if (typeof ngModelContoller === 'undefined') {
      return
    }

    return {
      'has-error': ngModelContoller.$invalid && ngModelContoller.$dirty,
      'has-success': ngModelContoller.$valid && ngModelContoller.$dirty
    };
  }

}

export default FormGroupController;