import templateUrl from './form-group.html';
import controller from './FormGroup.controller';

/**
 * Form Group directive
 * @returns {{restrict: string, replace: boolean, scope: {type: string, name: string, placeholder: string, formField: string, model: string, minLength: string, maxLength: string, match: string, disabled: string}, controllerAs: string, bindToController: boolean}}
 * @constructor
 */
function FormGroup() {
  let directive = {
    restrict: 'E',
    replace: true,
    scope: {
      type: '@',
      name: "@",
      placeholder: "@",
      formField: "=myFormField",
      model: "=ngModel",
      minLength: "=ngMinlength",
      maxLength: "=ngMaxlength",
      match: "=",
      disabled: '=ngDisabled'
    },
    controllerAs: 'formGroup',
    require: 'ngModel',
    bindToController: true,
    controller,
    templateUrl
  }

  return directive;
}

export default FormGroup;