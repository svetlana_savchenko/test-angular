import FormGroup from './form-group/FormGroup.directive';

export default angular
  .module('TestAngular.directives', [])
  .directive('formGroup', FormGroup)
  .name;