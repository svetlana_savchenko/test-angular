import Header from './header';
import Footer from './footer';
import SignIn from './signin';
import SignUp from './signup';
import Genre from './genre';
import Artist from './artist';

export default angular
  .module('TestAngular.Components', [
    Header,
    Footer,
    SignIn,
    SignUp,
    Genre,
    Artist
  ])
  .name