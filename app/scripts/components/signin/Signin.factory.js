/**
 * SigninFactory
 * @returns {{login: login}}
 * @constructor
 */
function SigninFactory() {
  return {
    login: login
  }

  /**
   * login
   * @param {Object} user
   * @returns {boolean}
   */
  function login(user) {
    // @TODO make login on the backend with tokens

    return user['email'] == localStorage['email']
      && user['password'] == localStorage['password'];
  }
}

export default SigninFactory;