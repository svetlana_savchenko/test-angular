/**
 * SigninController
 * @param redirect
 * @param SigninFactory
 * @constructor
 */
function SigninController(redirect, signinFactory, Notify) {
  const vm = this;

  vm.submit = submit;
  vm.cancel = cancel;


  /**
   * Submit registration form
   * @param {Object} fUser
   * @param {Object} user
   */
  function submit(fUser, user) {
    fUser.$valid === false
      ? showFormErrors(fUser)
      : login(user)
  }

  /**
   * Show Form errors
   * @param {Object} fUser
   */
  function showFormErrors(fUser) {
    //we should show only $error.required because the other errors
    // will be shown on key press into input fields
    if (fUser.$error.required) {
      fUser.$error.required.forEach((item) => item.$setDirty());
    }
  }

  /**
   * login
   * @param {Object} user
   */
  function login(user) {
    signinFactory.login(user)
      ? redirect('/', 'You are logged now.')
      : Notify.set('Email or password is wrong', 'error')
  }

  /**
   * Cancel registration form
   */
  function cancel() {
    redirect('/');
  }
}

SigninController.$inject = ['Redirect', 'SigninFactory', 'ngNotify']

export default SigninController;