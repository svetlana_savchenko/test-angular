import SigninConfig from './Signin.config';
import SigninFactory from './Signin.factory';

export default angular
  .module('TestAngular.Signin', ['ngRoute'])
  .factory('SigninFactory', SigninFactory)
  .config(SigninConfig)
  .name;