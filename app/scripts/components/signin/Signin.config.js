import controller from './Signin.controller';
import templateUrl from './signin.html';

/**
 * SigninConfig
 *
 * @param {Object} $routeProvider
 *
 * @return {Object}
 */
function SigninConfig($routeProvider) {
  $routeProvider
    .when('/signin', {
      controllerAs: 'signIn',
      bindToController: true,
      templateUrl,
      controller
    });
}

SigninConfig.$inject = [
  '$routeProvider'
];

export default SigninConfig;