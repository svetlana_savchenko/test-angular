import templateUrl from './header.html';

/**
 * HeaderDirective
 *
 * @return {Object}
 */
function HeaderDirective() {
  var directive = {
    restrict: 'E',
    templateUrl
  };

  return directive;
}

HeaderDirective.$inject = [];

export default HeaderDirective;