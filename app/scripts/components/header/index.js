import HeaderDirective from './Header.directive';
import HeaderMenuGuestDirective from './menu-guest/HeaderMenuGuest.directive';
import HeaderMenuUserDirective from './menu-user/HeaderMenuUser.directive';

export default angular
  .module('TestAngular.Header', [
  ])
  .directive('header', HeaderDirective)
  .directive('headerMenuGuest', HeaderMenuGuestDirective)
  .directive('headerMenuUser', HeaderMenuUserDirective)
  .name;