import templateUrl from './header-menu-guest.html';

/**
 * HeaderMenuGuestDirective
 *
 * @return {Object}
 */
function HeaderMenuGuestDirective() {
  var directive = {
    restrict: 'E',
    templateUrl
  };

  return directive;
}

export default HeaderMenuGuestDirective;