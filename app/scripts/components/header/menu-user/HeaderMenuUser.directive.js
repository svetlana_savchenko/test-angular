import templateUrl from './header-menu-user.html';

/**
 * HeaderMenuUserDirective
 *
 * @return {Object}
 */
function HeaderMenuUserDirective() {
  var directive = {
    restrict: 'E',
    templateUrl
  };

  return directive;
}

export default HeaderMenuUserDirective;