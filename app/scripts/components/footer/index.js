import directive from './Footer.directive'

export default angular
  .module('TestAngular.Footer', [])
  .directive('footer', directive)
  .name