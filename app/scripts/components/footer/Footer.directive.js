import templateUrl from './footer.html';

/**
 * FooterDirective
 * @constructor
 */
function FooterDirective() {
  let directive = {
    restrict: 'E',
    templateUrl
  };

  return directive;
}

export default FooterDirective;