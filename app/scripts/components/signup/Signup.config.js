import controller from './Signup.controller';
import templateUrl from './signup.html';

/**
 * SignupConfig
 *
 * @param {Object} $routeProvider
 *
 * @return {Object}
 */
function SignupConfig($routeProvider) {
  $routeProvider
    .when('/signup', {
      controllerAs: 'signUp',
      bindToController: true,
      templateUrl,
      controller
    });
}

SignupConfig.$inject = [
  '$routeProvider'
];

export default SignupConfig;