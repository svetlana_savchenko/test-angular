/**
 * SignupFactory
 * @returns {{register: register}}
 * @constructor
 */
function SignupFactory() {
  return {
    register: register
  }

  /**
   * Register user
   * @param user
   * @returns {*}
   */
  function register(user) {
    // @TODO register user on the server side
    localStorage['email'] = user['email'];
    localStorage['password'] = user['password'];

    return Promise.resolve(`${localStorage['email']}/${localStorage['password']}`)
  }
}

export default SignupFactory;