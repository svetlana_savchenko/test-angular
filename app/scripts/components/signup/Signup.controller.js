/**
 * SignupController
 * @param {Function} redirect
 * @param {Object} signupFactory
 * @constructor
 */
function SignupController(redirect, signupFactory) {
  const vm = this;
  
  vm.submit = submit;
  vm.cancel = cancel;


  /**
   * Submit registration form
   * @param {Object} fUser
   */
  function submit(fUser, user) {
    (fUser.$valid === true)
      ? register(user)
      : fUser.$error.required.forEach((item) => item.$setDirty());
  }

  /**
   * Register user
   * @param {Object} user
   */
  function register(user) {
    signupFactory.register(user)
      .then((userMsg) => redirect('/', `User created successfully. Try SignIn as ${ userMsg }.`));
  }

  /**
   * Cancel registration form
   */
  function cancel() {
    redirect('/');
  }
}

SignupController.$inject = ['Redirect', 'SignupFactory'];

export default SignupController;