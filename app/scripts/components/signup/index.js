import SignupConfig from './Signup.config';
import SignupFactory from './Signup.factory';

export default angular
  .module('TestAngular.Signup', ['ngRoute'])
  .factory('SignupFactory', SignupFactory)
  .config(SignupConfig)
  .name;