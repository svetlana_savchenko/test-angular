import GenreConfig from './Genre.config';
import GenreController from './Genre.controller';

export default angular
  .module('TestAngular.Genre', [
    'ngRoute'
  ])
  .controller('GenreController', GenreController)
  .config(GenreConfig)
  .name;