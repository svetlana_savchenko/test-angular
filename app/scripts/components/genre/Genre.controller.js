/**
 * GenreController
 * @constructor
 */
function GenreController() {
  const vm = this;

  vm.data = [
    {name: 'Rock'},
    {name: 'Pop'},
    {name: 'Jazz'},
    {name: 'Bluz'}
  ];

  vm.model = {
    filterGenre: ''
  };

  vm.setFilterGender = setFilterGender;
  vm.isActive = isActive;

  /**
   * Set filter for genres
   * @param {String} genderName
   */
  function setFilterGender(genderName) {
    this.model.filterGenre = genderName;
  }

  /**
   * Is element active
   * @param {String} genderName
   * @returns {{selected: boolean}}
   */
  function isActive(genderName) {
    return {
      selected: genderName === this.model.filterGenre
    }
  }
}

export default GenreController;