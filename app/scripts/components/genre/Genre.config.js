import controller from './Genre.controller';
import templateUrl from './genre.html';

/**
 * GenreConfig
 *
 * @param {Object} $routeProvider
 *
 * @return {Object}
 */
function GenreConfig($routeProvider) {
  $routeProvider
    .when('/', {
      controllerAs: 'genreCtrl',
      templateUrl,
      controller
    }).
    otherwise({
      redirectTo: '/'
    });
}

GenreConfig.$inject = [
  '$routeProvider'
];

export default GenreConfig;