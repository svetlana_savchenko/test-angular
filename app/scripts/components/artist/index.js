import ArtistDirective from './Artist.directive';

export default angular
  .module('TestAngular.Artist', [])
  .directive('artist', ArtistDirective)
  .name;