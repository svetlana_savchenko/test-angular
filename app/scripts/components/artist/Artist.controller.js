/**
 * ArtistController
 * @constructor
 */
function ArtistController(Confirm) {
  const vm = this;

  vm.data = [
    {id: 1, name: 'John Lennon', genre: 'Rock'},
    {id: 2, name: 'Gary Moore', genre: 'Rock'},
    {id: 3, name:'Steve Vai', genre: 'Rock'},
    {id: 4, name: 'Ronnie James Dio', genre: 'Rock'},
    {id: 5, name: 'Brithney Spears', genre: 'Pop'},
    {id: 6, name: 'lady gaga', genre: 'Pop'}
  ];

  vm.model = {
    idSelected: false,
    nameArtist: ''
  }

  vm.clickAddBtn = clickAddBtn;
  vm.clickDeleteBtn = clickDeleteBtn;
  vm.clickListArtist = clickListArtist;

  /**
   * Add artist
   * @param {String} filterGenre
   */
  function clickAddBtn(filterGenre) {
    let id = _getNextId();

    this.data.unshift({id: id, name: this.model.nameArtist, genre: filterGenre});
    this.model.nameArtist = '';
  }

  /**
   * Delete selected item
   */
  function clickDeleteBtn() {
    Confirm.open('Selected element will be deleted. Are you sure ?', _delete.bind(this));
  }

  /**
   * Select artist
   * @param {Object} event
   */
  function clickListArtist(event) {
    let id = +event.target.dataset.id,
      hasSelectedClass = this.model.idSelected === id;

    this.model.idSelected = hasSelectedClass ? false : id;
  }

  /**
   * Get next id for artists
   * @returns {number}
   * @private
   */
  function _getNextId() {
    let id = vm.data.reduce((max, item) => { return item.id > max ? item.id : max}, 0);
    return ++id;
  }

  /**
   * Delete selected element
   * @private
   */
  function _delete() {
    this.data.forEach((item, i) => (item.id === this.model.idSelected) ? delete this.data[i] : false);
    this.model.idSelected = false;
  }
}

ArtistController.inject = ['Confirm'];

export default ArtistController;