import templateUrl from './artist.html';
import controller from './Artist.controller';

/**
 * ArtistDirective
 * @returns {{restrict: string}}
 * @constructor
 */
function ArtistDirective() {
  var directive = {
    restrict: 'E',
    scope: {
      filterGenre: '='
    },
    controllerAs: 'artistCtrl',
    bindToController: true,
    templateUrl,
    controller
  };

  return directive;
}

export default ArtistDirective;