/**
 * NotifyConfig
 *
 * @param {Object} ngNotify
 *
 * @return undefined
 */
function NotifyConfig(ngNotify) {
  ngNotify.config({
    position: 'top',
    theme: 'pastel',
    duration: 3000,
    type: 'info',
    sticky: false
  });
}

NotifyConfig.$inject = ['ngNotify'];

export default NotifyConfig;