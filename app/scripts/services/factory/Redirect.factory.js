/**
 * Redirect
 *
 * @param {Object} $location
 * @param {Object} $injector
 *
 * @return {Object}
 */
function Redirect($location, $injector) {
  /**
   * @param {String} path
   * @param {String} message
   * @param {String} type
   * @param {Function} fn
   *
   * @return undefined
   */
  return function (path, message, type, fn) {
    var Notify = $injector.get('ngNotify');

    if (typeof message === 'string') {
      Notify.set(message, type || 'info');
    }

    $location.path(path);

    if (typeof fn === 'function') {
      fn.call(null);
    }
  };
}

Redirect.$inject = [
  '$location', '$injector'
];

export default Redirect;