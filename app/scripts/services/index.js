import Redirect from './factory/Redirect.factory';
import Confirm from './factory/Confirm.factory';
import NotifyConfig from './config/Notify.config';

export default angular
  .module('TestAngular.Services', [])

  .factory('Redirect', Redirect)
  .factory('Confirm', Confirm)

  .run(NotifyConfig)
  .name