import App from './config/application.js'


angular
  .element(document)
  .ready(() => angular.bootstrap(document, [App.name]));

